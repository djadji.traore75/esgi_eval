FROM ubuntu:20.10

RUN apt-get update && apt-get install -y --no-install-recommends \
    curl=7.68.0-1ubuntu4.2 \
    nginx=1.18.0-6ubuntu2 \
    git=1:2.27.0-1ubuntu1 \
    python3=3.8.6-0ubuntu1 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN addgroup esgi

RUN useradd -rm -d /home/user -s /bin/bash -g esgi -u 1001 jonathan
RUN useradd -rm -d /home/user -s /bin/bash -g esgi -u 1002 jordan

CMD ["sleep", "infinity"]